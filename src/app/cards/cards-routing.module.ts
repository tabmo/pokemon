import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { CardsListComponent } from './cards-list/cards-list.component';
import { CardResolver } from '../core/resolvers/card.resolver';


const routes: Routes = [
  {
    path: '',
    component: CardsListComponent
  },
  {
    path: ':id',
    component: CardDetailComponent,
    resolve: {
      card: CardResolver,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
