import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterComponent } from './filter.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { SearchService } from 'src/app/core/services/search.service';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { of } from 'rxjs';
import { Router } from '@angular/router';

describe('FilterComponent', () => {
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;
  let searchServiceStub: SearchService;
  const filtersStub = new SearchFilter();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterComponent ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;

    searchServiceStub = TestBed.inject(SearchService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('shout initialize filters types and supertypes', () => {
    (searchServiceStub as any).filters$ = of(filtersStub);
    const initTypesSpy = spyOn<any>(component, 'initTypes');
    const initSupertypesSpy = spyOn<any>(component, 'initSupertypes');

    component.ngOnInit();

    expect(component.filters).toEqual(filtersStub);
    expect(initTypesSpy).toHaveBeenCalled();
    expect(initSupertypesSpy).toHaveBeenCalled();
    expect(component.subscriptions.length).not.toBeNull();
  });

  it('should update filters and reset page to 1', async () => {
    const routerStub = TestBed.inject(Router);
    const navigateSpy = spyOn(routerStub, 'navigate').and.returnValue(Promise.resolve(true));
    const setFiltersSpy = spyOn(searchServiceStub, 'setFilters');
    component.filters = filtersStub;

    await component.search();

    expect(navigateSpy).toHaveBeenCalledWith(['/cards'], { queryParams: { page: 1 } });
    expect(setFiltersSpy).toHaveBeenCalledWith(filtersStub);
  });
});
