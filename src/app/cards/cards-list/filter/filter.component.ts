import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { SearchService } from 'src/app/core/services/search.service';
import { TypesService } from 'src/app/core/services/types.service';
import { SupertypesService } from 'src/app/core/services/supertypes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'tabmo-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit, OnDestroy {
  filters: SearchFilter;
  types: string[];
  supertypes: string[];
  subscriptions = [];

  constructor(
    private searchService: SearchService,
    private typesService: TypesService,
    private supertypesService: SupertypesService,
    private router: Router
  ) { }

  /**
   * Subscribe to filters$ in order to get filters in real time.
   * Initialize types and supertypes.
   */
  ngOnInit(): void {
    const subscription = this.searchService.filters$.subscribe((filters: SearchFilter) => this.filters = filters);
    this.subscriptions.push(subscription);
    this.initTypes();
    this.initSupertypes();
  }

  /**
   * After every change in filterForm reset page to 1 and update filters.
   */
  search = (): void => {
    this.router.navigate(['/cards'], { queryParams: { page: 1 } }).then(() => {
      this.searchService.setFilters(this.filters);
    });
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  /**
   * Call getTypes from typesService in order to initialize types.
   */
  private initTypes = (): void => {
    const subscription = this.typesService.getTypes().subscribe((data: { types: string[]; }) => {
      this.types = data.types;
    });
    this.subscriptions.push(subscription);
  }

  /**
   * Call getSupertypes from supertypesService in order to initialize supertypes.
   */
  private initSupertypes = (): void => {
    const subscription = this.supertypesService.getSupertypes().subscribe((data: { supertypes: string[]; }) => {
      this.supertypes = data.supertypes;
    });
    this.subscriptions.push(subscription);
  }
}
