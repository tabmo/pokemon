import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Card } from 'src/app/core/models/card';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { CardsService } from 'src/app/core/services/cards.service';
import { SearchService } from 'src/app/core/services/search.service';
import { PaginationService } from 'src/app/core/services/pagination.service';

@Component({
  selector: 'tabmo-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit, OnDestroy {
  cardsList: Card[];
  filters: SearchFilter;
  page: number;
  totalCount: number;
  subscriptions = [];

  constructor(
    private cardsService: CardsService,
    private paginationService: PaginationService,
    private searchService: SearchService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    /**
     * By default page = 1.
     */
    this.page = 1;

    this.initTotalCount();
  }

  /**
   * Subscribe to filters$ in order to get filters in real time.
   * Set filters and initialize page and cardsList.
   */
  ngOnInit(): void {
    const subscription = this.searchService.filters$.subscribe((filters: SearchFilter) => {
      this.filters = filters;
      this.setPage();
      this.getCards(this.filters);
    });
    this.subscriptions.push(subscription);
  }

  /**
   * Initialize the total number of elements (across all pages).
   */
  initTotalCount = () => {
    this.paginationService.totalCount.subscribe((totalCount: number) => {
      this.totalCount = totalCount;
    });
  }

  /**
   * Update page data and params when jumping to another page.
   */
  updatePage = (): void => {
    this.router.navigate([], { queryParams: { page: this.page } }).then(() => {
      window.scroll(0, 0);
      this.getCards(this.filters);
    });
  }

  /**
   * Set page by page queryParams value.
   */
  private setPage = (): void => {
    const subscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.page) {
        this.page = +params.page;
      }
    });
    this.subscriptions.push(subscription);
  }

  /**
   * Call getCards from cardsService in order to set cardsList.
   * @typeparam SearchFilter
   */
  private getCards = (filters: SearchFilter): void => {
    const subscription = this.cardsService.getCards(filters, this.page).subscribe((cards: Card[]) => {
      this.cardsList = cards;
    });
    this.subscriptions.push(subscription);
  }

  /**
   * Check if the page should display pagination.
   */
  isDisplayPagination = () => {
    if (this.cardsList && this.totalCount > this.filters.pageSize) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
