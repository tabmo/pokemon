import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CardsListComponent } from './cards-list.component';
import { SearchService } from 'src/app/core/services/search.service';
import { of } from 'rxjs';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { Router } from '@angular/router';
import { Card } from 'src/app/core/models/card';
import { NO_ERRORS_SCHEMA } from '@angular/core';


describe('CardsListComponent', () => {
  let component: CardsListComponent;
  let fixture: ComponentFixture<CardsListComponent>;
  let searchServiceStub: SearchService;
  let routerStub: Router;
  const cardsStub = [new Card()];
  const filtersStub = new SearchFilter();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardsListComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsListComponent);
    component = fixture.componentInstance;

    searchServiceStub = TestBed.inject(SearchService);
    // spyOnProperty(searchServiceStub, 'filters$', 'get').and.returnValue(of(filtersStub));
    (searchServiceStub as any).filters$ = of(filtersStub);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.page).toEqual(1);
  });

  it('shout initialize filters, set page and get cards', () => {
    const setPageSpy = spyOn<any>(component, 'setPage');
    const getCardsSpy = spyOn<any>(component, 'getCards');

    component.ngOnInit();

    expect(component.filters).toEqual(filtersStub);
    expect(setPageSpy).toHaveBeenCalled();
    expect(getCardsSpy).toHaveBeenCalled();
    expect(component.subscriptions.length).not.toBeNull();
  });

  it('should jump to another page and update cards', async () => {
    routerStub = TestBed.inject(Router);
    const navigateSpy = spyOn(routerStub, 'navigate').and.returnValue(Promise.resolve(true));
    const getCardsSpy = spyOn<any>(component, 'getCards');
    component.filters = filtersStub;
    component.page = 5;

    await component.updatePage();

    expect(navigateSpy).toHaveBeenCalledWith([], { queryParams: { page: 5 } });
    expect(getCardsSpy).toHaveBeenCalledWith(filtersStub);
  });

  it('should check if the page should display pagination', () => {
    let result = component.isDisplayPagination();

    expect(result).toBeFalse();

    component.cardsList = cardsStub;
    component.filters = filtersStub;
    component.filters.pageSize = 40;
    component.totalCount = 42;

    result = component.isDisplayPagination();

    expect(result).toBeTruthy();
  });
});
