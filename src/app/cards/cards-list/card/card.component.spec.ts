import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MyCartService } from 'src/app/core/services/my-cart.service';
import { Card } from 'src/app/core/models/card';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardComponent ],
      imports: [
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add card to shipping cart', () => {
    const cardStub = new Card();
    const myCartServiceStub = TestBed.inject(MyCartService);
    spyOn(myCartServiceStub, 'addCard');

    component.addCard(cardStub);

    expect(myCartServiceStub.addCard).toHaveBeenCalledWith(cardStub);
  });
});
