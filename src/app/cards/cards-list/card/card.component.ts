import { Component, OnInit, Input } from '@angular/core';
import { Card } from 'src/app/core/models/card';
import { MyCartService } from 'src/app/core/services/my-cart.service';

@Component({
  selector: 'tabmo-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card: Card;

  constructor(private myCartService: MyCartService) { }

  ngOnInit(): void {
  }

  /**
   * Add one card to my shipping cart by calling addCard from myCartService.
   */
  addCard = (card: Card) => {
    this.myCartService.addCard(card);
  }
}
