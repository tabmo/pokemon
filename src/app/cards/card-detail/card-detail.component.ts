import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/core/models/card';
import { ActivatedRoute } from '@angular/router';
import { MyCartService } from 'src/app/core/services/my-cart.service';

@Component({
  selector: 'tabmo-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss']
})
export class CardDetailComponent implements OnInit {
  card: Card;

  constructor(
    private activatedRoute: ActivatedRoute,
    private myCartService: MyCartService
  ) {}

  /**
   * Get card data from activatedRoute because we are using a resolver.
   */
  ngOnInit(): void {
    this.activatedRoute.data
      .subscribe((data: { card: Card }) => {
        this.card = data.card;
      });
  }


  /**
   * Add current card with a quantity to my cart.
   */
  addCard = (quantity: number) => {
    this.myCartService.addCard(this.card, quantity);
  }
}
