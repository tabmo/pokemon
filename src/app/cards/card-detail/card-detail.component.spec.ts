import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetailComponent } from './card-detail.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { Card } from 'src/app/core/models/card';
import { MyCartService } from 'src/app/core/services/my-cart.service';

describe('CardDetailComponent', () => {
  let component: CardDetailComponent;
  let fixture: ComponentFixture<CardDetailComponent>;
  const cardStub = new Card();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CardDetailComponent],
      imports: [
        RouterTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetailComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get card data from activated route', () => {
    let activatedRouteStub: ActivatedRoute;
    activatedRouteStub = TestBed.inject(ActivatedRoute);
    (activatedRouteStub as any).data = of({ card: cardStub });

    component.ngOnInit();

    expect(component.card).toEqual(cardStub);
  });

  it('should add current card with this quantity to shipping cart', () => {
    component.card = cardStub;
    let myCartServiceStub: MyCartService;
    myCartServiceStub = TestBed.inject(MyCartService);
    spyOn(myCartServiceStub, 'addCard');

    component.addCard(2);

    expect(myCartServiceStub.addCard).toHaveBeenCalledWith(cardStub, 2);
  });
});
