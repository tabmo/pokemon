import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {
  totalCount = new BehaviorSubject<number>(null);

  constructor() {}
}
