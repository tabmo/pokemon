import { TestBed } from '@angular/core/testing';

import { SupertypesService } from './supertypes.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SupertypesService', () => {
  let service: SupertypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(SupertypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
