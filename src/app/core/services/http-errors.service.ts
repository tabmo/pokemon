import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorsService {
  isErrorSubject = new BehaviorSubject(false);
  isError$: Observable<boolean> = this.isErrorSubject.asObservable();

  constructor() { }
}
