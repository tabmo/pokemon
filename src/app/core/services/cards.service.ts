import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Card } from '../models/card';
import { Observable } from 'rxjs';
import { SearchFilter } from '../models/search-filter';
import { map } from 'rxjs/operators';
import { PriceService } from '../helpers/price.service';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor(
    private http: HttpClient,
    private priceService: PriceService) { }

  /**
   * Call API in order to get cards.
   */
  getCards = (filter?: SearchFilter, page?: number): Observable<Card[]> => {
    const url = this.addParametersToUrl(filter, page);
    return this.http.get(url).pipe(
      map((data: { cards: Card[] }) => {
        return data.cards.map((card: Card) => {
          card.price = this.priceService.getPrice(card);
          return card;
        });
      })
    );
  }

  /**
   * Call API in order to get card by id.
   */
  getCardById = (id: string): Observable<Card> => {
    const url = `${environment.endpoint}cards/${id}`;
    return this.http.get(url).pipe(
      map((data: { card: Card }) => {
        const card = data.card;
        card.price = this.priceService.getPrice(card);
        return card;
      })
    );
  }

  /**
   * Helper to add parameters to requested url.
   */
  private addParametersToUrl = (filter?: SearchFilter, page?: number): string => {
    let url = `${environment.endpoint}cards/`;

    /**
     * By default page = 1.
     */
    if (page) {
      url = `${url}?page=${page}&pageSize=${filter.pageSize}`;
    } else {
      url = `${url}?page=1&pageSize=${filter.pageSize}`;
    }

    /**
     * Add a name as filter.
     */
    if (filter && filter.name) {
      url = `${url}&name=${filter.name}`;
    }

    /**
     * Add a supertypes ad filter.
     */
    if (filter && filter.supertype) {
      url = `${url}&supertype=${filter.supertype}`;
    }

    /**
     * Add a types as filter.
     */
    if (filter && filter.types) {
      url = `${url}&types=${filter.types}`;
    }

    return url;
  }
}
