import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CardWithQuantity } from '../models/card-with-quantity';
import { Card } from '../models/card';

@Injectable({
  providedIn: 'root'
})
export class MyCartService {
  private cardsList = [];
  private cardsSubject: BehaviorSubject<CardWithQuantity[]> = new BehaviorSubject<CardWithQuantity[]>([]);
  cards$: Observable<CardWithQuantity[]> = this.cardsSubject.asObservable();

  constructor() { }

  /**
   * Get the number of cards in my cart and the total amount.
   */
  getNbCardsAndTotal = (): { nbCards: number, total: number } => {
    let nbCards = 0;
    let total = 0;
    this.cardsList.forEach((card: CardWithQuantity) => {
      nbCards += card.quantity;
      total += Math.round(card.quantity * card.card.price * 100) / 100;
    });
    return { nbCards, total };
  }

  /**
   * Add a card to my cart or increase the quantity if card is already existing.
   * @typeparam Card.
   * @typeparam number (by default equal 1).
   */
  addCard = (card: Card, quantity = 1): void => {
    const cardWithQuantity = this.cardsList.find((c: CardWithQuantity) => {
      return c.card.id === card.id;
    });

    if (cardWithQuantity) {
      const index = this.cardsList.indexOf(cardWithQuantity);
      this.cardsList[index] = { ...this.cardsList[index], quantity: this.cardsList[index].quantity + quantity };
      this.cardsSubject.next(this.cardsList);
    } else {
      this.cardsList.push({ card, quantity });
      this.cardsSubject.next(this.cardsList);
    }
  }

  /**
   * Remove card from my cart or decrement the quantity if quantity is > 1.
   * @typeparam CardWithQuantity.
   */
  removeCard = (card: CardWithQuantity): void => {
    const index = this.cardsList.indexOf(card);
    if (index >= 0) {
      if (card.quantity > 1) {
        this.cardsList[index] = { ...card, quantity: --card.quantity };
        this.cardsSubject.next(this.cardsList);
      } else {
        this.cardsList.splice(index, 1);
        this.cardsSubject.next(this.cardsList);
      }
    }
  }

  /**
   * Delete card from my cart.
   * @typeparam CardWithQuantity.
   */
  deleteCard = (card: CardWithQuantity): void => {
    const index = this.cardsList.indexOf(card);
    if (index >= 0) {
      this.cardsList.splice(index, 1);
      this.cardsSubject.next(this.cardsList);
    }
  }


  /**
   * clean my cart by removing all cards.
   */
  cleanMyCart = (): void => {
    this.cardsList = [];
    this.cardsSubject.next(this.cardsList);
  }
}
