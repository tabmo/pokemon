import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SearchFilter } from '../models/search-filter';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private filtersSubject: BehaviorSubject<SearchFilter> = new BehaviorSubject<SearchFilter>(new SearchFilter());
  filters$: Observable<SearchFilter> = this.filtersSubject.asObservable();

  constructor() { }

  /**
   * Update filters.
   */
  setFilters = (filters: SearchFilter): void => {
    this.filtersSubject.next(filters);
  }
}
