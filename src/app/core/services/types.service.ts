import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypesService {

  constructor(private http: HttpClient) { }

  /**
   * Call API in order to get types
   */
  getTypes = (): Observable<{ types: string[]; }> => {
    return this.http.get<{ types: string[]; }>(`${environment.endpoint}types`);
  }
}
