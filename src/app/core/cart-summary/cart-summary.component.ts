import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CardWithQuantity } from '../models/card-with-quantity';
import { Card } from '../models/card';
import { MyCartService } from '../services/my-cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'tabmo-cart-summary',
  templateUrl: './cart-summary.component.html',
  styleUrls: ['./cart-summary.component.scss']
})
export class CartSummaryComponent implements OnInit {
  @Input() nbCards: number;
  @Input() total: number;
  @Input() cardsWhitQuantity: CardWithQuantity[];
  @Output() closeCartSummary = new EventEmitter();

  constructor(
    private myCartService: MyCartService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  /**
   * Increase the quantity of a chosen card in my cart.
   * @typeparam Card
   */
  addQuantity = (card: Card): void => {
    this.myCartService.addCard(card);
  }

  /**
   * Remove or decrement the quantity of a chosen card in my cart.
   * @typeparam CardWithQuantity
   */
  deleteCard = (card: CardWithQuantity): void => {
    this.myCartService.removeCard(card);
  }

  /**
   * Check if this is the last item in order to change card style.
   * @typeparam CardWithQuantity
   */
  isLastItem = (card: CardWithQuantity): boolean => {
    const index = this.cardsWhitQuantity.indexOf(card);
    if (index === this.cardsWhitQuantity.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Navigate to card detail page and close cart summary menu.
   * @typeparam string
   */
  goToCardDetail = (id: string) => {
    if (id) {
      this.router.navigate([`/cards/${id}`]).then(() => {
        this.closeCartSummary.emit();
      });
    }
  }

  /**
   * Navigate to my cart page and close cart summary menu.
   */
  goToMyCart = () => {
    this.router.navigate(['/my-cart']).then(() => {
      this.closeCartSummary.emit();
    });
  }

}
