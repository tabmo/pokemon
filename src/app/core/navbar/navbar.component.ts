import { Component, OnInit, ViewChild } from '@angular/core';
import { MyCartService } from '../services/my-cart.service';
import { CardWithQuantity } from '../models/card-with-quantity';
import { SearchService } from '../services/search.service';
import { SearchFilter } from '../models/search-filter';
import { Router } from '@angular/router';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'tabmo-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @ViewChild(NgbDropdown) private cartSummaryDropdown: NgbDropdown;
  nbCards: number;
  total: number;
  cardsWhitQuantity: CardWithQuantity[];
  filters: SearchFilter;

  constructor(
    private myCartService: MyCartService,
    private searchService: SearchService,
    private router: Router
  ) {
    this.nbCards = 0;
    this.total = 0;
  }

  /**
   * Initialize cardsWhitQuantity, nbCards, total and filters.
   */
  ngOnInit(): void {
    this.myCartService.cards$.subscribe((cards: CardWithQuantity[]) => {
      this.cardsWhitQuantity = cards;
      const { nbCards, total } = this.myCartService.getNbCardsAndTotal();
      this.nbCards = nbCards;
      this.total = Math.round(total * 100) / 100;
    });

    this.searchService.filters$.subscribe((filter: SearchFilter) => this.filters = filter);
  }

  /**
   * Reset filters and navigate to cards page.
   */
  navigateToCardsPage = () => {
    this.router.navigate(['/cards']).then(() => {
      this.filters = new SearchFilter();
      this.searchService.setFilters(this.filters);
    });
  }

  /**
   * Navigate to /cards page and show data according to my filters.
   */
  search = (): void => {
    this.router.navigate(['/cards'], { queryParams: { page: 1 }} ).then(() => {
      this.searchService.setFilters(this.filters);
    });
  }

  /**
   * Close cart summary manually.
   */
  closeCartSummary = () => {
    this.cartSummaryDropdown.close();
  }

}
