import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PaginationService } from '../services/pagination.service';

@Injectable()
export class PaginationInterceptor implements HttpInterceptor {
  constructor(private paginationService: PaginationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {

      if (event instanceof HttpResponseBase) {
        if (event.headers.has('total-count')) {

          const totalCount: number = JSON.parse(event.headers.get('total-count'));
          this.paginationService.totalCount.next(totalCount);

        }
      }
    }));
  }

}
