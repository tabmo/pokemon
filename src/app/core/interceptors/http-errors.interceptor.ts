import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorsService } from '../services/http-errors.service';


@Injectable()
export class HttpErrorsInterceptor implements HttpInterceptor {

  constructor(private httpErrorsService: HttpErrorsService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.httpErrorsService.isErrorSubject.next(false);

    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {
        if (this.isHttpResponseError(err)) {
          this.httpErrorsService.isErrorSubject.next(true);
        }

        // return the error stream
        return throwError(err);
      }));
  }

  /**
   * @return true if the response is in a global error state (not functional error state)
   */
  private isHttpResponseError(response: HttpErrorResponse): boolean {
    return response instanceof HttpErrorResponse && response.status >= 400 && response.status !== 409;
  }
}
