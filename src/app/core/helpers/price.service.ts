import { Injectable } from '@angular/core';
import { Card } from '../models/card';

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  constructor() { }

  /**
   * get card price
   * @typeparam Card
   */
  getPrice = (card: Card): number => {
    if (card.hp && card.hp !== 'None') {
      return this.calculatePrice(card);
    } else {
      return 0.99;
    }
  }

  /**
   * Calculate card price.
   * @typeparam Card
   */
  private calculatePrice = (card: Card): number => {
    return Math.round(+card.hp * 0.02 * 100) / 100;
  }
}
