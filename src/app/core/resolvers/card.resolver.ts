import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Card } from '../models/card';
import { CardsService } from '../services/cards.service';


@Injectable()
export class CardResolver implements Resolve<Card> {
  constructor(private cardsService: CardsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Card> {
    const cardId = route.paramMap.get('id');
    return this.cardsService.getCardById(cardId);
  }
}
