export class Attacks {
    cost: string[];
    name: string;
    text: string;
    damage: string;
    convertedEnergyCost: number;
}
