import { Attacks } from './attacks';
import { Resistances } from './resistances';
import { Weaknesses } from './weaknesses';

export class Card {
    id: string;
    name: string;
    price: number;
    imageUrl: string;
    imageUrlHiRes: string;
    supertype: string;
    subtype: string;
    number: string;
    artist: string;
    rarity: string;
    series: string;
    set: string;
    setCode: string;
    hp?: string;
    nationalPokedexNumber?: number;
    types?: string[];
    retreatCost?: string[];
    convertedRetreatCost?: string;
    text?: string[];
    attacks?: Attacks[];
    resistances?: Resistances[];
    weaknesses?: Weaknesses[];
}
