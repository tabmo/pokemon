export class SearchFilter {
    name: string = null;
    supertype: string = null;
    types: string = null;
    pageSize = 40;
}
