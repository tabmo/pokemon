import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { CartSummaryComponent } from './cart-summary/cart-summary.component';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { LoaderComponent } from './loader/loader.component';
import { HttpErrorsInterceptor } from './interceptors/http-errors.interceptor';
import { CardResolver } from './resolvers/card.resolver';
import { QuantityFormComponent } from './quantity-form/quantity-form.component';
import { PaginationInterceptor } from './interceptors/pagination.interceptor';

@NgModule({
  declarations: [
    NavbarComponent,
    NotFoundComponent,
    CartSummaryComponent,
    LoaderComponent,
    QuantityFormComponent
  ],
  exports: [
    NavbarComponent,
    LoaderComponent,
    QuantityFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    CardResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorsInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: PaginationInterceptor,
      multi: true
    }
  ]
})
export class CoreModule { }
