import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantityFormComponent } from './quantity-form.component';
import { FormsModule } from '@angular/forms';

describe('QuantityFormComponent', () => {
  let component: QuantityFormComponent;
  let fixture: ComponentFixture<QuantityFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuantityFormComponent ],
      imports: [
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantityFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
