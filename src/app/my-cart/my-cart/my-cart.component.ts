import { Component, OnInit } from '@angular/core';
import { MyCartService } from 'src/app/core/services/my-cart.service';
import { Router } from '@angular/router';
import { CardWithQuantity } from 'src/app/core/models/card-with-quantity';
import { Card } from 'src/app/core/models/card';

@Component({
  selector: 'tabmo-my-cart',
  templateUrl: './my-cart.component.html',
  styleUrls: ['./my-cart.component.scss']
})
export class MyCartComponent implements OnInit {
  nbCards: number;
  total: number;
  cardsWhitQuantity: CardWithQuantity[];

  constructor(
    private myCartService: MyCartService,
    private router: Router
  ) { }

  /**
   * Initialize cardsWhitQuantity, nbCards and total.
   */
  ngOnInit(): void {
    this.myCartService.cards$.subscribe((cards: CardWithQuantity[]) => {
      this.cardsWhitQuantity = cards;
      const { nbCards, total } = this.myCartService.getNbCardsAndTotal();
      this.nbCards = nbCards;
      this.total = Math.round(total * 100) / 100;
    });
  }

  /**
   * Increase the quantity of a chosen card in my cart.
   * @typeparam Card
   */
  increaseQuantity = (card: Card): void => {
    this.myCartService.addCard(card);
  }

  /**
   * Remove or decrease the quantity of a chosen card in my cart.
   * @typeparam CardWithQuantity
   */
  decreaseOrDelete = (card: CardWithQuantity): void => {
    this.myCartService.removeCard(card);
  }

  /**
   * Delete card from my cart.
   * @typeparam CardWithQuantity
   */
  deleteCard = (card: CardWithQuantity): void => {
    this.myCartService.deleteCard(card);
  }

}
