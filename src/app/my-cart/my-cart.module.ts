import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyCartRoutingModule } from './my-cart-routing.module';
import { MyCartComponent } from './my-cart/my-cart.component';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [MyCartComponent],
  imports: [
    CommonModule,
    MyCartRoutingModule,
    CoreModule
  ]
})
export class MyCartModule { }
